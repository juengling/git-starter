% Git - Einführung
% Christoph Jüngling
% 2018-01-22 (#92636ce)

-----------

Dieses Dokument beschreibt **Git** und **TortoiseGit** (graphische Oberfläche für Git) unter Windows. Git gibt es auch für andere Betriebssysteme, TortoiseGit meines Wissens jedoch nur für Windows. Die Git-Befehle auf der Kommandozeile ("Konsole") sind in allen Betriebssystemen identisch, abgesehen natürlich von einigen konsolenspezifischen Besonderheiten.

Ich versuche in diesem Dokument, die Git-Schlüsselworte (und nur diese) in **Fettdruck** zu setzen, andere Hervorhebungen in *kursiv*. Befehle, Datei- und Verzeichnisnamen werden in `Monospace-Font` gesetzt.

\newpage
# Basis

## Repositories

Git ist ein *dezentrales* Quellcodeverwaltungssystem. Das bedeutet, dass es immer ein *lokales Repository* (technisch ist dies das `.git`-Verzeichnis) gibt. Alle außer `.git` noch im Projektverzeichnis vorhandene Dateien und Ordner gehören zum sog. *Arbeitsverzeichnis*.

![Git-Verzeichnis](Folder.png "Git-Verzeichnis")

Zusätzlich können beliebig viele *entfernte Repositories* mit dem lokalen Repository verknüpft werden. Mit diesen können dann Commits ausgetauscht werden. Git kann problemlos auch mit mehreren entfernten Repositories zusammen arbeiten, wobei das lokale Repository sogar unterschiedliche Stände und Branches der anderen Repositories abbilden kann. Ein Beispiel wäre ein Entwicklungs- und ein Produktions-Server.

Aufgrund dieser geteilten Struktur wird zwischen einem **Commit** (nur lokal) und einem **Pull** bzw. **Push** unterschieden.

- Ein **Commit** fügt die dafür vorgemerkten Änderungen und einige organisatorische Details in das Log ein. 
- Ein **Pull** holt den aktuellen Stand von einem entfernten Repository in das lokale Repository und aktualisiert das Arbeitsverzeichnis entsprechend.
- Ein **Push** schickt Änderungen aus dem lokalen Repository an ein (oder mehrere) entferntes Repository.
- Ein **Fetch** ist wie ein **Pull**, jedoch wird das Arbeitsverzeichnis dabei nicht aktualisiert.
 

## Commits

**Commit** ist ein Begriff, der leider für zwei Dinge verwendet wird. Zum einen ist es der Befehl (wie oben beschrieben), zum anderen dessen Ergebnis, also ein Änderungsdatensatz. Ein Commit (Änderungsdatensatz) kann beliebig viele Dateien und Änderungen enthalten. Allerdings ist es aus Gründen der Übersichtlichkeit empfohlen, dies nicht zu übertreiben.

Man muss keineswegs nach jedem Absatz ein **Commit** machen, ebenso wie man
nicht nach jeder Codezeile **committen** muss. Aber man kann es tun, wenn es aus
inhaltlicher Sicht sinnvoll erscheint, z.B. um ein *Refactoring* zur Verbesserung der Lesbarkeit von der nachfolgenden *inhaltlichen Änderung* zu trennen.


## Branches

Das Konzept der **Branches** ist in Git sehr wichtig. Alle Commits sind in Branches organisiert. Falls der Entwickler von diesem Konezept keinen Gebrauch macht, wird es auf jeden Fall einen Branch geben, der in Git üblicherweise **master** heißt.

Branches müssen nicht grundsätzlich auch auf irgend einen Server gepusht werden. Es ist im Gegenteil sogar üblich, Änderungen lokal auf einem temporären Branch (im Beispiel "Abschlussvorstellung") durchzuführen, der dann später wieder in den **master**-Branch (der Hauptentwicklungszweig) zurückgeführt werden. Danach kann und sollte der Arbeitsbranch gelöscht werden.
 

# TortoiseGit

[Git](https://git-scm.com/) ist ein Kommandozeilenprogramm, [TortoiseGit](https://tortoisegit.org/) eine graphische Oberfläche dafür.  

## Log-Darstellung
Im Bild "Farben" sind sowohl der "Current Branch", als auch der "Local Branch" beides lokale Branches. 

![Farben](TGit-Branch-Farben.png "Farben in TortoiseGit")

Im folgenden Beispiel "Git-Log" ist der **master-Branch** bereits zu einem entfernten Repository (Git: **remote**) **gepusht** worden und mit diesem auf einem gemeinsamen Stand. Da er grün dargestellt wird, ist er nicht der aktuelle Branch.

![Git-Log](Git-Log.png "Git-Log")

Der **Branch** "Abschlussvorstellung" ist rot dargestellt und daher der **aktive Branch** (auf diesem wird gerarbeitet), was bedeutet, dass das Arbeitsverzeichnis dessen Stand mehr oder weniger enthält. "Mehr oder weniger", weil es durchaus auch schon weiter sein kann, aber eben noch nicht **committet** ist. Außerdem ist erkennbar, dass dieser **Branch** nur lokal vorhanden ist, denn dort ist kein hellbraunes Label zu sehen.

Wenn die Arbeiten an diesem Branch abgeschlossen sind, sollten sie in den **master-Branch** überführt werden. Dazu sind folgende Schritte nötig:

1. **master-Branch** auschecken (TortoiseGit-Befehl `Switch/Checkout`)
2. Rechtsklick auf den Branch "Abschlussvorstellung"
3. Auswahl des Kommandos `Merge to "master"`
4. Ausführen der Aktion
5. Links unten auf `Delete Branch "Abschlussvorstellung"` klicken 

Das Ergebnis sieht dann aus wie in dem Bild "Git-Log nach dem Merge". Danach ist dann irgendwann ein **push** zu dem entfernten Repository sinnvoll. Dieses bekommt aber nach wie vor die "Abschlussvorstellung" nicht zu sehen, nur deren Ergebnisse.

![Git-Log-2](Git-Log-2.png "Git-Log nach dem Merge")


# Fragen

Wie sehe ich, was im Repository drin ist?
